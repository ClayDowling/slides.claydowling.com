# slides.claydowling.com

Conference slides, visible at [slides.claydowling.com](http://slides.claydowling.com).

Please feel free to use these slides for your own purposes, with proper attribution, as required by the [BSD License](LICENSE)