---
title: "C Ecosystem"
weight: 200
---

{{< slide background="/images/backgrounds/nuts.jpg" >}}

# Ecosystem

Many common problems.

Many common solutions.

---

## XML

* _libxml2_ - Fast parser using document or sax style.
* _xpat_ - Light weight sax parser.
* _Apache Xerces_ - __Do Not Use__
* _Microsoft XML_ - __Do Not Use__

---

## Json

* _cJSON_ - github.com/DaveGamble/cJSON - Light, fast, easy.
* _jansson_ - github.com/akheron/jansson - Full featured, good examples.

---

## Network/REST Client

### libcurl

If it's a network protocol, libcurl speaks it.

Easy to mock/test code using libcurl.

----


## REST Server

### libulfius

Easy to test without mocking.

Can have a server doing useful work in under an hour.

---

Database

Native, fast clients available for:

* PostgreSQL
* MySQL
* SQLite
* Redis
* etc...

---

## Game Programming

* An infinite array of tools
* SDL is simple and well supported

---

## Write Your Own Language

* lex/flex to tokenize your input.
* bison/lemon to parse your language grammar.
* an old, well-solved problem.