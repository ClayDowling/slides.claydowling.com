---
title: "Avoid Allocating Memory"
weight: 110
---

## Avoid Allocating Memory

Prefer

    struct stat sb;
    if (stat("file.txt", &sb))

Over

    struct stat *sb = calloc(1, sizeof(struct stat));
    if (stat("file.txt", sb))
