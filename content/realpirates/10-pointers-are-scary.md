---
title: "Pointers are Scary"
weight: 100
---

# Pointers are Scary

> Programming is like playing with a loaded gun. C cocks the hammer for you.

<footer>
  <cite>&mdash;Anonymous IBM Employee</cite>
</footer>
