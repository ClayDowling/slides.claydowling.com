---
title: "Technical Reasons"
weight: 30
---

## Technical Reasons

* Functional - Functions are first class objects
* Object Oriented - Data and logic separated by default
* Test Driven Development - Built in test features and robust libraries
* Web Native - Libraries for easy self hosted web apps
* Distributed - Libraries for remote interaction
