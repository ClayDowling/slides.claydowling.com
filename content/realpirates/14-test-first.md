---
title: "Test First"
weight: 140
---

{{< slide background="/images/backgrounds/petri-dish.jpg" >}}

# Test First

---

## Trust Nothing

Write automated tests for all of your code

Use valgrind on your tests

Do not ignore valgrind errors

---

## You Will F*** Stuff Up

"I am a mere mortal, and that's okay."

![Ferry aground](/images/slides/ferry-aground.jpg)

---

## Sample Test

    START_TEST(skillcmp_givenAandB_returnsNegativeOne) {
        skill_t *a = skill_create("A", 0, S_CATEGORY, S_ATTRIBUTE);
        skill_t *b = skill_create("B", 0, S_CATEGORY, S_ATTRIBUTE);
        
        ck_assert_int_eq(-1, skill_cmp(a, b));
        
        skill_destroy(a);
        skill_destroy(b);
    }
    END_TEST

---

## Test Run

    ./test-genesyssheet
    Unity test run 1 of 1
    .....

    -----------------------
    5 Tests 0 Failures 0 Ignored
    OK

---

## Make Tests Part of Your Build

    all: test product

    product: $(OBJECTS) main.o
        $(CC) -o $@ $^
        
    test: test-product
        ./test-product
        valgrind test-product

    test-product: $(TEST_OBJECTS) $(OBJECTS)
        $(CC) -o $@ $^ $(TEST_LIBS)
