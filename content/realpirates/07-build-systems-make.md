---
title: "Make"
weight: 70
---

## Make

    PROGNAME=myprogram
    OBJS=$(patsubst %.c, %.o, $(wildcard *.c))
    
    $(PROGNAME): $(OBJS)
        $(CC) -o $@ $^

    clean:
        rm -f $(PROGNAME)
        rm -f *.o