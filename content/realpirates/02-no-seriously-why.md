---
title: "No, Seriously, Why?"
weight: 20
---

## No, Seriously, Why?

* Small and easy to understand
* Rich ecosystem
* 50 years of language evolution
* Real development with minimal tools