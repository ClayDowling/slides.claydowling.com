---
title: "Fixing Your Culture"
weight: 80
---

{{< slide background="/images/backgrounds/emptyoffice.jpg" >}}

# Fixing Your Culture

---

# You Are a Leader

Leadership doesn't come with a title.  Leadership is how you help other people.

---

> If I speak but do not have love, I am only a clanging cymbal.

<cite>I Corinthians 13:1</cite>
