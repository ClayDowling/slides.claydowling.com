---
title: "Known Helpers"
weight: 110
---

{{% slide background="/images/backgrounds/welder.jpg" %}}

# Fix Your Work

---

## Make the Work Engaging

- Connect the work to the consumer.
- Keep tasks small.

<cite>[Intrinsic Motivation at Work](https://amzn.to/2GCSWol)</cite>

---

## Small Tasks

- Easy to track progress
- Easy to spot risks

---

## Share Your Work

- Show work in progress to consumers to get regular feedback
- Prioritize work to show visible value

---

## Surface Problems Early

*Covering Your Ass* lies about the true situation.

Micromanagement will follow.
