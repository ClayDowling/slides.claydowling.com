---
title: "Team Care"
weight: 100
---
# Clock Punchers

* Work 40 hours.
* Have priorities other than work.
* Insufficiently dedicated to the team.
* Passed over for promotions, raises, and preferable work assignments.

---

## Be a Clock Puncher

* Don't be part of the problem.
* It's a job, not a suicide cult.
* Leave on time.
* Invite reluctant colleagues out to lunch.

---

# Build a Team of Clock Punchers

---

## Team Accountability

We must all hang together, or we shall surely hang individually.

---

## Make Sure the Work Gets Done

* Make sure management has visibility into work status
* Make sure management is aware of risks and problems
* Make work units small enough to understand and report on easily

---
## Bring Problems to the Team

If the team owns the problems, the team will also own the results.

---
## The Team Owns The Work

* No individual work assignments
* There is nothing beneath your pay grade

---
## Self Care is Best Care

* Take care of yourself
* You are your brother's keeper

---
## Management Can't Watch You 24/7

If the work is getting done, management won't care if you're in the office or not.
