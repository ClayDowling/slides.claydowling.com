---
title: "The Ideal Worker"
weight: 15
---

{{% slide background="/images/backgrounds/worker-bees.jpg" %}}

## The Ideal Worker

* Fully available for the job
* No personal responsibilities
* 60-80 hour weeks
* No say of when or where work is done

<cite>Harvard Business Review [Why Some Men Pretend to Work 80 Hour Weeks](
https://hbr.org/2015/04/why-some-men-pretend-to-work-80-hour-weeks?fbclid=IwAR3maBT76R1KA5OP3I4a83lo-0JC0UgrFKLPQpTH1c1OsrVlwkOXobtWvr4)</cite>

---

# 57%

[Percentage of workers reporting job burnout](http://blog.teamblind.com/index.php/2018/05/29/close-to-60-percent-of-surveyed-tech-workers-are-burnt-out-credit-karma-tops-the-list-for-most-employees-suffering-from-burnout/).

---

## Burnout is Expensive

[$125M - $190M a year in medical expenses](https://hbswk.hbs.edu/item/national-health-costs-could-decrease-if-managers-reduce-work-stress)

---

# What Can I Do?