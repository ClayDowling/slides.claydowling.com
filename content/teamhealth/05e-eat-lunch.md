---
title: "Eat Your Lunch"
weight: 58
draft: true
---

## Let's Talk About Lunch

Numbers from [Tork Takes Back the Lunch Break](https://www.torkusa.com/brand/torktakesback/)

---
# 20%

of North American worker worry their bosses won’t think they are hardworking if they take regular lunch breaks. 13% worry their coworkers will judge them.

---

# 22%

of North American bosses think
that employees who take a
regular lunch break are less
hardworking.

---

# 7%

More likely to be satisfied with your own performance if you take a lunch.

---

# 6%

More likely to be satisfied with your job if you take a lunch.

---

# 3%

Less likely to change jobs tommorow if the opportunity existed.

---

## Takeaway

* Take your lunch
* Not at your desk

