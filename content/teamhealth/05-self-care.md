---
title: "Self Care"
weight: 50
---
{{% slide background="/images/backgrounds/kayakinggroup.jpg" %}}

# Self Care is Best Care

---
![Danger](/images/slides/Achtung.png)

Career Limiting Advice Follows

---
## Fourty Hours, No More

- Health Suffers
- Work Suffers

---

## Long Hours Kill

Working 55 hours or more per week:

- 13% more likely to have a heart attack
- 33% more likely to have a stroke

<cite>[Only the overworked die young](https://www.health.harvard.edu/blog/only-the-overworked-die-young-201512148815)</cite>

---

## Long Hours are Bad Business

Reducing the work day from 9 hours to 8 hours gives better than 10% boost in productivity.

---

> "Ernst Abbe, the head of one of the greatest German factories, wrote many years ago that the shortening from nine to eight hours, that is, a cutting-down of more than 10 per cent, did not involve a reduction of the day's product, but an increase"

<cite>Hugo Muensterberg, "Psychology and Industrial Efficiency" (1913)</cite>

---
## Work is not Life

![Thea Disapproves of My Code](/images/slides/thea-watching-me-code.jpg)

- Have a place to go away from work.
- Make sure your cat knows that you live there.

---

## Workaholism

> the stable tendency of excessive and compulsive working

---

## Bosses Love Workaholics

<cite>[Workaholism...may also negatively affect the work environment.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4117275/)</cite>

---

### Are You a Workaholic?

<small>

1. You think of how you can free up more time to work.
1. You spend much more time working than initially intended.
1. You work in order to reduce feelings of guilt, anxiety, helplessness or depression.
1. You have been told by others to cut down on work without listening to them.
1. You become stressed if you are prohibited from working.
1. You deprioritize hobbies, leisure activities, and/or exercise because of your work.
1. You work so much that it has negatively influenced your health.

</small>

<cite>[Workaholism tied to psychiatric disorders](http://www.sciencedaily.com/releases/2016/05/160525084547.htm)</cite>

---

## The Wake

![Things they don't tell you'll that you'll have to organize as a tech lead](/images/slides/kevin-culp-wake.jpg)

---
## Your Body Wants to Move

You don't need to run marathons.  But it wouldn't hurt.

---
## Balance, Not Balance Balls

* A standing desk is not a substitute for a walk
* [Sunlight reduces depression](https://www.ncbi.nlm.nih.gov/pubmed/19016463)