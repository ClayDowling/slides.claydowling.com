---
title: "Clay Dowling's Presentations"
outputs: ["Reveal"]
---

* [Team Health for Fun and Profit](/teamhealth)
* [Real Pirates Seek the C!](/realpirates)